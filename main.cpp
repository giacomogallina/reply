#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <bitset>
#include <deque>
#include <algorithm>
#include <tuple>

using namespace std;

#define MAX_ROW 1024
#define MAX_COL 1024

int righe, colonne;

const int dev_chair = 0;
const int pm_chair = 1;
const int wall = 2;

int matrice[MAX_ROW][MAX_COL];

typedef unsigned long long ull;

struct pers {
		int company;
		int bonus;
		bitset<1024> skills;
		bool is_pm;
		bool piazzato;
};

vector<pers> people;

int points(pers a, pers b) {
		int common = (a.skills & b.skills).count();
		int disjoint = (a.skills ^ b.skills).count();
		return common * disjoint + (a.company == b.company ? a.bonus * b.bonus : 0);
}

int max(int a, int b) { return a > b ? a : b; }

int flor[MAX_ROW][MAX_COL];
int av_pm, orig_av_pm;
int av_dev, orig_av_dev;

pair<int, int> rand_spot(bool is_pm) {
		int r, c;
		while (1) {
				r = rand() % righe;
				c = rand() % colonne;
				if (flor[r][c] == -1 && matrice[r][c] == (is_pm ? pm_chair : dev_chair)) {
						return {r, c};
				}
		}
}

vector<pair<int, int>> pos;

int main(int argc, char *argv[]) {
		ifstream fin(argv[1]);
		fin >> colonne >> righe;

		for(int i = 0; i < righe; i++) {
				for (int j = 0; j < colonne; j++) {
						char c;
						fin >> c;
						if (c == '_') {
								matrice[i][j] = dev_chair;
								flor[i][j] = -1;
								av_dev += 1;
						} else if (c == 'M') {
								matrice[i][j] = pm_chair;
								flor[i][j] = -1;
								av_pm += 1;
						} else {
								matrice[i][j] = wall;
								flor[i][j] = -2;
						}
				}
		}

		orig_av_pm = av_pm;
		orig_av_dev = av_dev;

		int comp_n = 0;

		int dev_n;
		fin >> dev_n;
		for (int i = 0; i < dev_n; i++) {
				int c, b, skn;
				fin >> c >> b >> skn;
				comp_n = max(comp_n, c);
				bitset<1024> sk;

				for (int j = 0; j < skn; j++) {
						int s;
						fin >> s;
						sk.set(s);
				}
				people.push_back({ c, b, sk, false, false });
		}

		int pm_n;
		fin >> pm_n;
		for (int i = 0; i < pm_n; i++) {
				int c, b;
				fin >> c >> b;
				comp_n = max(comp_n, c);
				bitset<1024> sk;
				people.push_back({ c, b, sk, true, false });
		}

		//cout << "ok" << endl;


		deque<int> devs_of_comp[comp_n];
		deque<int> pms_of_comp[comp_n];

		for (int i = 0; i < people.size(); i++) {
				if (people[i].is_pm) {
					pms_of_comp[people[i].company].push_back(i);
				} else {
					devs_of_comp[people[i].company].push_back(i);
				}
		}

		//cout << "ok" << endl;

		for (int i = 0; i < comp_n; i++) {
				sort(pms_of_comp[i].begin(), pms_of_comp[i].end(), [](int a, int b) {
								return people[a].bonus > people[b].bonus;
				});
				sort(devs_of_comp[i].begin(), devs_of_comp[i].end(), [](int a, int b) {
								return people[a].bonus > people[b].bonus;
				});
		}

		//while (20*av_spots > righe*colonne) {
				//int i, j;
				//tie(i, j) = rand_spot(people[orig_av_spots - av_spots].is_pm);
				//flor[i][j] = 1;
				//cout << i << " " << j << endl;
				//av_spots -= 1;
		//}

		//int x = 0, y = 0;
		//for (int c = 0; c < comp_n; c++) {
				//for (auto i: devs_of_comp[c]) {
						//if (!av_dev) {
								//break;
						//}
						//while (flor[x][y] != -1) {
								//y += 1;
								//if (y == colonne) {
										//y = 0;
										//x += 1;
								//}
						//}

				//}
		//}

		for (int p = 0; p < people.size(); p++) {
				int i, j;
				if (people[p].is_pm) {
						if (30*av_pm > righe*colonne) {
								tie(i, j) = rand_spot(people[p].is_pm);
								flor[i][j] = 1;
								cout << j << " " << i << endl;
								av_pm -= 1;
						} else {
								cout << "X" << endl;
						}
				} else {
						if (30*av_dev > righe*colonne) {
								tie(i, j) = rand_spot(people[p].is_pm);
								flor[i][j] = 1;
								cout << j << " " << i << endl;
								av_dev -= 1;
						} else {
								cout << "X" << endl;
						}
				}
		}
		

		//  DEBUGGING 

		//for (int i = 0; i < righe; i++) {
			//for (int j = 0; j < colonne; j++) {
					//cout << matrice[i][j];
			//}
			//cout << endl;
		//}

		//for (auto p: people) {
				//cout << p.company << endl;
				//cout << p.bonus << endl;
				//cout << p.skills << endl;
				//cout << p.is_pm << endl;
				//cout << endl << endl;
		//}

		//while (true) {
				//int a, b;
				//cin >> a >> b;
				//cout << points(people[a], people[b]) << endl;
		//}


		return 0;
}
