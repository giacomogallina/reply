#!/bin/bash

g++ main.cpp -o main
./main ./a_solar.txt > output_a.txt
./main ./b_dream.txt > output_b.txt
./main ./c_soup.txt > output_c.txt
./main ./d_maelstrom.txt > output_d.txt
./main ./e_igloos.txt > output_e.txt
./main ./f_glitch.txt > output_f.txt
