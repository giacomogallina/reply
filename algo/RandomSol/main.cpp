#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <random>
#include <ctime>

using namespace std;

#define MAX_ROW 1024
#define MAX_COL 1024

int righe, colonne;

const int dev_chair = 0;
const int pm_chair = 1;
const int wall = 2;

using ii = pair<int,int>;

int main(int argc, char *argv[]) {
		ifstream fin(argv[1]);
		fin >> colonne >> righe;

		vector<ii> dd,mm;

		srand(time(0));

		for(int i = 0; i < righe; i++) {
				for (int j = 0; j < colonne; j++) {
						char c;
						fin >> c;
						if (c == '_') {
								dd.push_back({i,j});		
						} else if (c == 'M') {
								mm.push_back({i,j});		
						}
				}
		}

		int dev_n;
		fin >> dev_n;
		for (int i = 0; i < dev_n; i++) {
				int c, b, skn;
				fin >> c >> b >> skn;
				for (int j = 0; j < skn; j++) {
						int s;
						fin >> s;
				}
		}

		int pm_n;
		fin >> pm_n;
		for (int i = 0; i < pm_n; i++) {
				int c, b;
				fin >> c >> b;
		}

		random_shuffle(dd.begin(), dd.end());
		random_shuffle(mm.begin(), mm.end());

		int k;

		k=0;
		for(int i=0;i<dd.size() && k<dev_n;i++){
			ii it = dd[i];
			printf("%d %d\n", it.second, it.first);
			k++;
		}
		for(int i=k;k<dev_n;k++){
			printf("X\n");
		}



		k=0;
		for(int i=0;i<mm.size() && k<pm_n;i++){
			ii it = mm[i];
			printf("%d %d\n", it.second, it.first);
			k++;
		}
		for(int i=k;k<pm_n;k++){
			printf("X\n");
		}

}
