#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <bitset>
#include <deque>
#include <algorithm>

using namespace std;

#define MAX_ROW 1024
#define MAX_COL 1024

int righe, colonne;

const int dev_chair = 0;
const int pm_chair = 1;
const int wall = 2;

int matrice[MAX_ROW][MAX_COL];

typedef unsigned long long ull;

struct pers {
		int company;
		int bonus;
		bitset<1024> skills;
		bool is_pm;
		bool piazzato;
};

vector<pers> peopleD, peopleM;

vector<pers> pD[1024], pM[1024];


int points(pers a, pers b) {
		int common = (a.skills & b.skills).count();
		int disjoint = (a.skills ^ b.skills).count();
		return common * disjoint + (a.company == b.company ? a.bonus * b.bonus : 0);
}

int max(int a, int b) { return a > b ? a : b; }

int main(int argc, char *argv[]) {
		ifstream fin(argv[1]);
		fin >> righe >> colonne;

		for(int i = 0; i < righe; i++) {
				for (int j = 0; j < colonne; j++) {
						char c;
						fin >> c;
						if (c == '_') {
								matrice[i][j] = dev_chair;
						} else if (c == 'M') {
								matrice[i][j] = pm_chair;
						} else {
								matrice[i][j] = wall;
						}
				}
		}

		int comp_n = 0;

		int dev_n;
		fin >> dev_n;
		for (int i = 0; i < dev_n; i++) {
				int c, b, skn;
				fin >> c >> b >> skn;
				comp_n = max(comp_n, c);
				bitset<1024> sk;

				for (int j = 0; j < skn; j++) {
						int s;
						fin >> s;
						sk.set(s);
				}
				peopleD.push_back({ c, b, sk, false, false });
		}

		int pm_n;
		fin >> pm_n;
		for (int i = 0; i < pm_n; i++) {
				int c, b;
				fin >> c >> b;
				comp_n = max(comp_n, c);
				bitset<1024> sk;
				peopleM.push_back({ c, b, sk, true, false });
		}

//

		for(auto it : peopleD){
			pD[it.company].push_back(it);
		}

		for(auto it : peopleM){
			pM[it.company].push_back(it);
		}

}
