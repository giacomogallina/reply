#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

void sighandler(int);

/**Usage:
 * quando la matrice matrice[][]
 * 
 * 
 * 
 **/

bool stop;

int compute_contribution(int row, int column, int maxrow, int maxcolumn) {
	int sum = 0;
	
	if (flor[row][column] < 0) return -1;
	pers person = people[flor[row][column]];
	pers persona2;
	
	if (row > 0 and column > 0) {
		if (flor[row-1][column-1] >= 0) {
			persona2 = people[flor[row-1][column-1]];
			sum += points(person, persona2);
		}
	}
	if (row < maxrow-1 and column > 0) {
		if (flor[row+1][column-1] >= 0) {
			persona2 = people[flor[row+1][column-1]];
			sum += points(person, persona2);
		}
	}
	if (row > 0 and column < maxcolumn -1) {
		if (flor[row-1][column+1] >= 0) {
			persona2 = people[flor[row-1][column+1]];
			sum += points(person, persona2);
		}
	}
	if (row < maxrow-1 and column < maxcolumn -1) {
		if (flor[row+1][column+1] >= 0) {
			persona2 = people[flor[row+1][column+1]];
			sum += points(person, persona2);
		}
	}
	return sum;
}

int permuter () {
	stop=false;
	signal(SIGINT, sighandler);
	int maxrow = righe;
	int maxcolumn = colonne;
	bool free_manag = false;
	bool free_devel = false;
	int i, j, k, l;
	bool is_pm;
	
	for (i=0;i<people.size();i++) {
		if (people[i].piazzato == false) {
			if (people[i].is_pm)
				free_manag = true;
			else
				free_devel = true;
		}
		if (free_manag and free_devel) break;
	}
	
	//prova a cambiare la gente in campo con quella in panchina
	while(stop==false) {
		int current_sum, current_person;
		int new_sum, new_person;
		for (i=0;i<maxrow;i++) {
			for (j=0;j<maxcolumn;j++) {
				current_person = flor[i][j];
				is_pm = people[flor[i][j]].is_pm;
				current_sum = compute_contribution(i, j,maxrow,maxcolumn);
				for (k=0;k<people.size();k++) {
					if (people[k].is_pm == is_pm and !(people[k].piazzato)) {
						flor[i][j]=k;
						new_sum = compute_contribution(i, j, maxrow, maxcolumn);
						if (new_sum > current_sum) {
							people[k].piazzato = true;
							people[current_person].piazzato = false;
						} else {
							flor[i][j] = current_person;
						}
					}
				}
				if (stop==true) break;
			}
			if (stop==true) break;
		}
	
	//prova a scambiare la gente in campo
		int a_sum, b_sum, temp_index;
		for (i=0;i<maxrow;i++) {
			for (j=0;j<maxcolumn;j++) {
				if (flor[i][j]<0) continue;
				for (k=0;k<maxrow;k++) {
					for (l=0;l<maxcolumn;j++) {
		if (flor[k][l]<0) continue;
		if (people[flor[i][j]].is_pm==people[flor[k][l]].is_pm) {
			a_sum = compute_contribution(i,j,maxrow,maxcolumn);
			if (a_sum<0) continue;
			b_sum = compute_contribution(k,l,maxrow,maxcolumn);
			if (b_sum<0) continue;
			current_sum = a_sum + b_sum;
			temp_index = flor[i][j];
			flor[i][j] = flor[k][l];
			flor[k][l] = temp_index;
			a_sum = compute_contribution(i,j,maxrow,maxcolumn);
			b_sum = compute_contribution(k,l,maxrow,maxcolumn);
			new_sum = a_sum + b_sum;
			if (new_sum>current_sum) continue;
			else {
				temp_index = flor[i][j];
				flor[i][j] = flor[k][l];
				flor[k][l] = temp_index;
			}
		}
					}
				}
				if (stop==true) break;
			}
			if (stop==true) break;
		}
		
	
	}
	return(42); //Se ritorna l'universo è finito
}

void sighandler(int signum) {
	stop=true;
}

